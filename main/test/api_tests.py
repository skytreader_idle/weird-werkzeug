from flask.ext.testing import TestCase

import main
import logging
import dateutil.parser
import unittest
import json

class MainTestCase(TestCase):
    
    def create_app(self):
        return main.main

    def test_servertime(self):
        servertime = self.client.get("/servertime")
        self.assertEquals(servertime._status_code, 200)
        spam = json.loads(servertime.data)
        self.assertTrue(dateutil.parser.parse(spam["now"]))

if __name__ == "__main__":
    unittest.main()
