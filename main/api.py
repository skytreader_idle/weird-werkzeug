from datetime import datetime
from flask import Blueprint
import pytz

main_api = Blueprint("main_api", __name__)

@main_api.route("/servertime")
def servertime():
    return {"now": str(datetime.now(tz=pytz.utc).isoformat())}
