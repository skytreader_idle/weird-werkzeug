from flask import Flask

main = Flask(__name__)

from api import main_api

main.register_blueprint(main_api)
